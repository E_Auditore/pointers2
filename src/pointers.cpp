#include <iostream>

using namespace std;


int main()
{
  int x = 25, y = 2 * x;    
  auto a = &x, b = a;
  auto c = *a, d = 2 * *b;


  cout<<"The value of x = "<<x<<"and the address = "<<&x<<endl;
  cout<<"The value of y = "<<y<<"and the address = "<<&x<<endl;
  cout<<"The value of a = "<<*a<<"and the address = "<<a<<endl;
  cout<<"The value of b = "<<*b<<"and the address = "<<b<<endl;
  cout<<"The value of c = "<<c<<"and the address = "<<&c<<endl;
  cout<<"The value of d = "<<d<<"and the address = "<<&d<<endl;

 



}
